﻿using System;
using System.Collections.Generic;
using System.Linq;
using LINQtoCSV;
using QuChallenge.Infrastructure.Models;

namespace QuChallenge.Infrastructure.Utilities
{
    public class CsvReader
    {
        public static IEnumerable<string> ReadCsvFile(string filePath)
        {
            var csvFileDescription = new CsvFileDescription
            {
                FirstLineHasColumnNames = true
            };

            var csvContext = new CsvContext();
         
            return csvContext.Read<SingleColumnFile>(filePath).Select(x => x.Values);
        }
    }
}
