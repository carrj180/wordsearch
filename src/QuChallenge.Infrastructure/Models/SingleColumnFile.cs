﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuChallenge.Infrastructure.Models
{
    public class SingleColumnFile
    {
        public string Values { get; set; }
    }
}
