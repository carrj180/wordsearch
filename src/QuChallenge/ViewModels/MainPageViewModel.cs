﻿using System.Collections.Concurrent;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using QuChallenge.Applications.Queries;
using QuChallenge.Applications.QueryHandler;
using QuChallenge.Models;

namespace QuChallenge.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private ConcurrentDictionary<string, InformationFile> _cache =
            new ConcurrentDictionary<string, InformationFile>();

        private IEnumerable<string> _wordsFound;
        public IEnumerable<string> WordsFound
        {
            get => _wordsFound;
            set
            {
                if (_wordsFound == value) return;
                _wordsFound = value;
                OnPropertyChanged();
            }
        }

        public Command UploadMatrixCommand { get; }
        public Command UploadWordListCommand { get; }
        public Command FindWordsListCommand { get; }

        public MainPageViewModel()
        {
            UploadMatrixCommand = new Command(UploadFile);
            UploadWordListCommand = new Command(UploadWordList);
            FindWordsListCommand = new Command(WordList);
            FindWordsListCommand = new Command(WordList);
        }

        private void WordList()
        {
            var matrixContent = Enumerable.Empty<string>();
            var wordsContent = Enumerable.Empty<string>();

            if (_cache.TryGetValue("Matrix", out var matrixResult))
            {
                var query = new ReadFileQuery(matrixResult.Path ?? string.Empty);
                var queryHandler = new ReadFileQueryHandler();
                matrixContent = queryHandler.Handle(query).Content;
            }

            if (_cache.TryGetValue("ListToFind", out var listToFindResult))
            {
                var query = new ReadFileQuery(listToFindResult.Path ?? string.Empty);
                var queryHandler = new ReadFileQueryHandler();
                wordsContent = queryHandler.Handle(query).Content;
            }

            var wordsFinderQuery = new WordFinderQuery(matrixContent, wordsContent);
            var wordsFinderQueryHandler = new WordFinderQueryHandler();
            WordsFound = wordsFinderQueryHandler.Handler(wordsFinderQuery);
        }

        private async void UploadFile()
        {
            await CaptureFile("Matrix");
        }

        private async void UploadWordList()
        {
            await CaptureFile("ListToFind");
        }


        private async Task CaptureFile(string fileIdentifier)
        {
            var fileResult = await FilePicker.PickAsync(new PickOptions
            {
                PickerTitle = "Select a file to upload"
            });

            if (fileResult != null)
            {
                var informationFile = new InformationFile
                {
                    Name = fileResult.FileName,
                    Path = fileResult.FullPath
                };

                _cache.TryAdd(fileIdentifier, informationFile);
            }
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
