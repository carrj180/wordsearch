﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuChallenge.Models
{
    public class InformationFile
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
