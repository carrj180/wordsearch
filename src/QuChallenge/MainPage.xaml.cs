﻿using QuChallenge.ViewModels;

namespace QuChallenge;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
        BindingContext = new MainPageViewModel();
    }
}

