﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuChallenge.Domain.Models
{
    public class FileContent
    {
        public string FilePath { get; }
        public IEnumerable<string> Content { get; }

        public FileContent(string filePath, IEnumerable<string> content)
        {
            FilePath = filePath;
            Content = content;
        }
    }
}
