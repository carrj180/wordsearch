﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuChallenge.Domain.Models
{
    public class WordFinder
    {
        public IEnumerable<string> Matrix { get; }
        public IEnumerable<string> Words { get; }

        public WordFinder(IEnumerable<string> matrix, IEnumerable<string> words)
        {
            Matrix = matrix;
            Words = words;
        }
    }
}
