﻿using QuChallenge.Applications.Queries.Interfaces;
using QuChallenge.Domain.Models;

namespace QuChallenge.Applications.Queries
{
    public class WordFinderQuery : IQuery<WordFinder>
    {
        public IEnumerable<string> Matrix { get; }
        public IEnumerable<string> Words { get; }

        public WordFinderQuery(IEnumerable<string> matrix, IEnumerable<string> words)
        {
            Matrix = matrix;
            Words = words;
        }
        
    }
}
