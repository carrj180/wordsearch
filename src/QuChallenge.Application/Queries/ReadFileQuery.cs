﻿
using QuChallenge.Applications.Queries.Interfaces;
using QuChallenge.Domain.Models;

namespace QuChallenge.Applications.Queries
{
    public class ReadFileQuery : IQuery<FileContent>
    {
        public string FilePath { get; }

        public ReadFileQuery(string filePath)
        {
            FilePath = filePath;
        }
    }
}
