﻿using System.Collections.Concurrent;
using QuChallenge.Applications.Queries;
using QuChallenge.Domain.Models;

namespace QuChallenge.Applications.QueryHandler
{
    public class WordFinderQueryHandler : IQueryHandler<WordFinderQuery, WordFinder>
    {
        public IEnumerable<string> Handler(WordFinderQuery query)
        {
            var rows = query.Matrix.Count();
            var cols = query.Matrix.First().Length;
            var wordsListConcurrently = new ConcurrentBag<string>();

            var charMatrix = new char[rows, cols];
            var row = 0;

            query.Matrix.ToList().ForEach(line =>
            {
                for (var col = 0; col < cols; col++)
                {
                    charMatrix[row, col] = line[col];
                }

                row++;
            });

            var taskHorizontal = Task.Run(() => SearchHorizontally(charMatrix, query.Words.ToArray(), wordsListConcurrently));
            var taskVertical = Task.Run(() => SearchVertically(charMatrix, query.Words.ToArray(), wordsListConcurrently));

            Task.WaitAll(taskHorizontal, taskVertical);

            return wordsListConcurrently.ToList();

        }

        public void SearchHorizontally(char[,] matrix, string[] wordsToFind, ConcurrentBag<string> wordsList)
        {
            var size = matrix.GetLength(0);

            for (var index = 0; index < wordsToFind.Length; index++)
            {
                var rowString = "";
                var word = wordsToFind[index];
                for (var row = 0; row < size; row++)
                {
                    var tempColumn = 0;
                    for (var col = 0; col < size; col++)
                    {
                        rowString += matrix[row, col];
                        tempColumn = col;
                    }

                    if (!rowString.Contains(word, StringComparison.InvariantCultureIgnoreCase)) continue;

                    wordsList.Add(word);
                    Console.WriteLine($"Word '{word}' found horizontally in row {row + 1}, column: {size - tempColumn}");
                    break;
                }
            }
        }

        public void SearchVertically(char[,] matrix, string[] wordsToFind, ConcurrentBag<string> wordsList)
        {
            var size = matrix.GetLength(0);

            for (var index = 0; index < wordsToFind.Length; index++)
            {
                var colString = "";
                var word = wordsToFind[index];
                for (var col = 0; col < size; col++)
                {
                    var tempRow = 0;
                    for (var row = 0; row < size; row++)
                    {
                        colString += matrix[row, col];
                        tempRow = row;
                    }

                    if (!colString.Contains(word, StringComparison.InvariantCultureIgnoreCase)) continue;

                    wordsList.Add(word);
                    Console.WriteLine($"Word '{word}' found vertically in column {col + 1}, row: {size - tempRow}");
                    break;
                }
            }
        }
    }
}
