﻿using QuChallenge.Applications.Queries;
using QuChallenge.Domain.Models;
using QuChallenge.Infrastructure.Utilities;
using System.Formats.Asn1;

namespace QuChallenge.Applications.QueryHandler
{
    public class ReadFileQueryHandler: IQueryHandler<ReadFileQuery, FileContent>
    {
        public FileContent Handle(ReadFileQuery query)
        {
            var fileContent = CsvReader.ReadCsvFile(query.FilePath);
            
            return new FileContent(query.FilePath, fileContent);
        }
    }
}
