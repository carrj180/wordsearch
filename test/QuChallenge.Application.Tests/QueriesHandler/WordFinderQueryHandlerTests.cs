﻿using NUnit.Framework;
using QuChallenge.Applications.Queries;
using QuChallenge.Applications.QueryHandler;

namespace QuChallenge.Application.Tests.QueriesHandler
{
    [TestFixture]
    public class WordFinderQueryHandlerTests
    {
        [Test]
        public void Handle_ValidQuery_ReturnsExpectedResults()
        {
            // Arrange
            var matrix = new List<string>
            {
                "abcuc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy"
            };

            var words = new List<string>
            {
                "abc",
                "chill",
                "pq"
            };

            var query = new WordFinderQuery(matrix, words);
            var handler = new WordFinderQueryHandler();

            // Act
            var results = handler.Handler(query);

            // Assert
            var expectedResults = new List<string>
            {
                "abc",
                "chill",
                "pq"
            };

            Assert.That(results, Is.EquivalentTo(expectedResults));
        }

        [Test]
        public void Handle_NoMatchingWords_ReturnsEmptyList()
        {
            // Arrange
            var matrix = new List<string>
            {
                "abcuc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy"
            };

            var words = new List<string>
            {
                "xyz",
                "efg"
            };

            var query = new WordFinderQuery(matrix, words);
            var handler = new WordFinderQueryHandler();

            // Act
            var results = handler.Handler(query);

            // Assert
            Assert.That(results, Is.Empty);
        }
    }
}
