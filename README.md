# QU Chanllenge MAUI App

This is a sample .NET MAUI (Multi-platform App UI) application built with CQRS architecture.

## Overview

The application follows the CQRS pattern, which separates the read and write operations into separate components. This provides a clear separation of concerns and improves scalability and maintainability.

The project structure is organized as follows:

- QuChallenge.Domain: Contains the core domain logic and domain models. (Scaffolding)
- QuChallenge.Application: Implements the application services, command handlers, and query handlers.
- QuChallenge.Infrastructure: Provides the infrastructure layer, including data access, event handling, and external service integrations. (Scaffolding)
- QuChallenge.UI: Contains the UI layer, including the user interfaces, views, and view models. (Scaffolding)
- QuChallenge.**.Tests: Contains NUnit test cases for the each layer.

## Prerequisites

Before running the application, ensure that the following prerequisites are met:

- .NET 6 SDK (or later) is installed.
- MAUI 7 (or later) is installed.
- NUnit test framework is installed (can be added via NuGet).

## Getting Started

1. Clone the repository: https://gitlab.com/carrj180/wordsearch.git
2. Open the solution file in VS
3. Pick the Windows Machine profile and run

## Who it's works

![Windows](documents/screen_1.png)
- Select the matrix file as a csv file clicking on Upload matrix
![Windows](documents/screen_2.png)
- Select the words list to find as a csv file clicking on Upload words to search
![Windows](documents/screen_3.png)
- Start to find the words clicking on Start to Find button
![Windows](documents/screen_4.png)

## GitLab CI/CD Prerequisites

- Docker: Install Docker on your machine. Refer to the [official Docker documentation](https://docs.docker.com/get-docker/) for installation instructions.
- A custome image was created for configure the environment required for the gitlab-ci.yml for Android and Windows target.
- The images jcarr1801578/challenge_words_finder:latest is located in the docker hub registry.

## GitLab CI/CD Pipeline

The CI/CD pipeline is defined in the `.gitlab-ci.yml` file. It consists of the following stages:

### 1. Validate

This stage validates the project configuration and dependencies.

- Linting and code style checks
- Dependency checks

### 2. Build

This stage builds the project artifacts.

- Building source code
- Compiling assets
- Packaging application

### 3. Test

This stage runs the unit tests and integration tests.

- Running unit tests
- Collecting code coverage (not available yet)